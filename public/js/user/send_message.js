$(document).ready(function() {
    $(document).on("submit",".contact-form",function(event) {
        let $this = $(this);
        event.preventDefault();
        
        var form_data = $this.serializeArray();
        var data = {};
        for(let i = 0; i < form_data.length; i++) {
            data[form_data[i].name] = form_data[i].value;
        }
        $.ajax({
            url : $this.attr("action"),
            type : $this.attr("method"),
            data : data,
            dataType : 'json',
            success : function(response) {
                $("#myModal .modal-body p").html(response.message);
                $("#myModal").modal("show");
                $('#myModal').on('hidden.bs.modal', function (e) {
                    $this.trigger("reset");
                });
            },
            error : function(error) {
                console.log(error);
            } 
        });
    });
});