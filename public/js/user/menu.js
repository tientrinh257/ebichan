$(document).ready(function() {
    const current_menu = $("#current_menu").val();
    $(`.nav-item[data-name='${current_menu}']`).addClass("active");

    // When the user scrolls down 80px from the top of the document, resize the navbar's padding and the logo's font size
    function scrollFunction() {
        if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
            $('#btn_move_top').fadeIn();
        } else {
            $('#btn_move_top').fadeOut();
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        var body = $("html, body");
        body.stop().scrollTop(0);
    }
    
    window.onscroll = function() {scrollFunction()};

    $(document).on('click','#btn_move_top', function(){
        topFunction();
    });

    $(document).on('click',"#menu .nav-link", function() {
        $(".navbar-toggler:visible").trigger("click");
    });
});
