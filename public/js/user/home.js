$(document).ready(function () {
    // AOS.init();

    function isElementInViewport(elem) {
        var $elem = $(elem);
    
        // Get the scroll position of the page.
        // var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'body' : 'html');
        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();
    
        // Get the position of the element on the page.
        var elemTop = Math.round( $elem.offset().top );
        var elemBottom = elemTop + $elem.height();
        // console.log(((elemTop < viewportBottom) && (elemBottom > viewportTop)));
        return ((elemTop < viewportBottom) && (elemBottom > viewportTop));
    }
    
    // Check if it's time to start the animation.
    function checkAnimation() {
        var $elem = $('.workshop-wrap .circle');
    
        // If the animation has already been started
        if ($elem.hasClass('start')) return;
    
        if (isElementInViewport($elem)) {
            // Start the animation
            $elem.addClass('start');
        }
    }
    
    // Capture scroll events
    $(window).scroll(function(){
        checkAnimation();
    });

});