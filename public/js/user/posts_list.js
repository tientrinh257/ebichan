$(document).ready(function() {
    const base_url = $("#base_url").val();
    GetList();

    function GetList() {
        const page_offset = $('#page_offset').attr('page_offset');
        $('#page_offset').attr('page_offset',0);
        $.ajax({
            type: 'post',
            data: {
                page_offset : page_offset
            },
            url: `${base_url}user/posts/partial_list`,
            success: function (data) {
                $("#posts-list").html(data);
            }
        });
    }
});