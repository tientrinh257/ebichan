$(document).ready(function() {
    const month_array = ["1","2","3","4","5","6","7","8","9","10","11","12"];
    const base_url = $("#base_url").val();
    
    function load_side_news(data) {
        const mydate = new Date(data.create_datetime); 
        const month = month_array[mydate.getMonth()];
        const day = mydate.getDate();
        const year = mydate.getFullYear();
        const news_url = data.language == "vietnamese" ? "tin-tuc" : "news";
        const news_html = 
        `<li class="news-topic">
            <a href="${base_url}${news_url}/${data.url}">${data.title}</a>
            <p class="text-muted small mt-1"><i class="far fa-clock"></i> ${day}-${month}-${year}</p>
        </li>`;
        $(".news-sidebar ol").append(news_html).children(':last').hide().fadeIn(1000);

    }

    $(document).on("click",".btn-load-more", function() {
        let offset = $(".news-topic").length;
        const btn = $(this);
        $.ajax({
            url: `${base_url}user/news/side_load_more`,
            type : "post",
            dataType : 'json',
            data : {
                offset : offset
            },
            success : function(response) {
                if( response.length == 0 ) {console.log(12312);
                    $(btn).attr("disabled",true);
                } else {
                    for(let i = 0; i < response.length; i++) {
                        load_side_news(response[i]);
                    }
                }
            },
            error : function(error) {
                console.log(error);
            } 
        });
    });

});