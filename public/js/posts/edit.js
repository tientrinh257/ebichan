$(document).ready(function() {
    const base_url = $("#base_url").val();
    const main_url = $("#main_url").val();
    DecoupledEditor
		.create( document.querySelector( '#editor' ), {
			// toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
			ckfinder: {
                uploadUrl: base_url+'/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
            }
		} )
		.then( editor => {
			const toolbarContainer = document.querySelector( '.toolbar-container' );

			toolbarContainer.prepend( editor.ui.view.toolbar.element );

			window.editor = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );

    $(document).on('submit', '#edit', function(e){
        showLoading();
        e.preventDefault();
        var form = $(this);
        var formdata = false;
        if (window.FormData){
			formdata = new FormData(form[0]);
        }
        formdata = formdata ? formdata : form.serialize();
		let content = editor.getData();
		formdata.append("content",content);
        var formAction = form.attr('action');
        $.ajax({
            url         : formAction,
            data        : formdata,
            cache       : false,
            contentType : false,
            processData : false,
            type        : 'POST',
            dataType    : 'json',
            success     : function(response) {
                hideLoading();
                $("#myModal .modal-body p").html(response.message);
                $("#myModal").modal("show");
            },
            error : function(error) {
                console.log(error);
            } 
        });
    } );

    $(document).on('click','.close_model',function() {
        location.href = `${base_url}${main_url}`;
    });

    $(document).on("click",".close", function() {
        let img_class = $(this).parent().find("img").attr("class");
        $(this).parent().find("img").remove();
        $(this).parent().append(`<input type="file" class="form-control" name="${img_class}" accept="image/*" />`);
        $(this).remove();
    });
});