function showLoading(){
    $('#waitting').show();
    $('<div></div>').prependTo('body').attr('id', 'overlay');
}
function hideLoading(){
    $('#waitting').hide();
    var overlay = $('#overlay');
    if(overlay){
      overlay.remove();
    }
}