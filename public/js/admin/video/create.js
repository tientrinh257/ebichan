$(document).ready(function() {
    const main_url = $("#main_url").val();
    const base_url = $("#base_url").val();

    $(document).on('submit', '#add', function(e){
        showLoading();
        e.preventDefault();
        var form = $(this);
        var formdata = false;
        if (window.FormData){
            formdata = new FormData(form[0]);
        }
    
        var formAction = form.attr('action');
        $.ajax({
            url         : formAction,
            data        : formdata ? formdata : form.serialize(),
            cache       : false,
            contentType : false,
            processData : false,
            type        : 'POST',
            dataType    : 'json',
            success     : function(response) {
                hideLoading();
                $("#myModal .modal-body p").html(response.message);
                $("#myModal .modal-footer .add_another").removeAttr("hidden");
                $("#myModal").modal("show");
            },
            error : function(error) {
                console.log(error);
            }
        });
    });

    $(document).on('click','.close_model',function() {
        
        location.href = `${base_url}${main_url}`;
    });

    $(document).on('click','.add_another',function() {
        document.getElementById("add").reset();
        
        $("#myModal .modal-footer .add_another").attr("hidden","hidden");
    });

});