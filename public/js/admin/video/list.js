$(document).ready(function() {
    $(document).on("click","input.delete",function() {
        let $this = $(this);
        let r = confirm("Delete post ?");
        if(r) {
            location.href = $this.attr("data-url");
        }
    });
});