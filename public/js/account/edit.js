$(document).ready(function() {
    const base_url = $("#base_url").val();
    const main_url = $("#main_url").val();

    $(document).on('submit', '#edit', function(e){
        showLoading();
        e.preventDefault();
        var form = $(this);
        var formdata = false;
        if (window.FormData){
            formdata = new FormData(form[0]);
        }
        formdata = formdata ? formdata : form.serialize();
        var formAction = form.attr('action');
        $.ajax({
            url         : formAction,
            data        : formdata,
            cache       : false,
            contentType : false,
            processData : false,
            type        : 'POST',
            dataType    : 'json',
            success     : function(response) {
                hideLoading();
                $("#myModal .modal-body p").html(response.message);
                $("#myModal").modal("show");
            },
            error : function(error) {
                console.log(error);
            } 
        });
    } );

    $(document).on('click','.close_model',function() {
        location.href = `${base_url}admin/logout`;
    });
});