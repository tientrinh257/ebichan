<?php
class MY_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // Ten table
	var $table = '';

	// Key chinh cua table
	var $key = 'id';

	// Order mac dinh (VD: $order = array('id', 'desc))
	//sap xep gia tri
	var $order = '';

	// Cac field select mac dinh khi get_list (VD: $select = 'id, name')
	var $select = '';


	/**
	 * Them row moi
	 * $data : du lieu ma ta can them
	 */
	function create($data = array())
	{
		if($this->db->insert($this->table, $data))
		{
		   return TRUE;
		}else{
			return FALSE;
		}
	}

    function update_rule($where, $data)
	{
		if (!$where){
			return FALSE;
		}
	 	$this->db->where($where);
	 	if($this->db->update($this->table, $data)) {
			return TRUE;
		} else {
			return FALSE;
		}
	 	
	}

	/**
	 * Cap nhat row tu id
	 * $id : khoa chinh cua bang can sua
	 * $data : mang du lieu can sua
	 */
	function update($id, $data)
	{
		if (!$id)
		{
			return FALSE;
		}

		$where = array();
	 	$where[$this->key] = $id;
	    $this->update_rule($where, $data,$this->table);

	 	return TRUE;
	}
    function del_rule($where)
	{
		if (!$where)
		{
			return FALSE;
		}

	 	$this->db->where($where);
		$this->db->delete($this->table);

		return TRUE;
	}
	/**
	 * Xoa row tu id
	 * $id : gia tri cua khoa chinh
	 */
	function delete($id)
	{
		if (!$id)
		{
			return FALSE;
		}
		//neu la so
		if(is_numeric($id))
		{
			$where = array($this->key => $id);
		}else
		{
		    //xoa nhieu row
		    //$id = 1,2,3...
			$where = $this->key . " IN (".$id.") ";
		}
	 	$this->del_rule($where, $this->table);

		return TRUE;
	}
    /**
	 * Lay thong tin cua row tu dieu kien
	 * $where: Mảng điều kiện
	 * $field: Cột muốn lấy dữ liệu
	 */
	function get_info_rule($where = array(), $field= '')
	{
	    if($field)
	    {
	        $this->db->select($field);
	    }
		$this->db->where($where);
		$query = $this->db->get($this->table);
		if ($query->num_rows())
		{
			return $query->row_array();
		}

		return FALSE;
	}
	/**
	 * Lay thong tin cua row tu id
	 * $id : id can lay thong tin
	 * $field : cot du lieu ma can lay
	 */
	function get_info($id, $field = '')
	{
		if (!$id)
		{
			return FALSE;
		}

	 	$where = array();
	 	$where[$this->key] = $id;

	 	return $this->get_info_rule($where, $field);
	}
	/**
	 * Lay tong so
	 */
	function get_total($input = array())
	{
		$this->get_list_set_input($input);

		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	/**
	 * Lay danh sach
	 * $input : mang cac du lieu dau vao
	 */
	function get_list($input = array())
	{
	    //xu ly ca du lieu dau vao
		$this->get_list_set_input($input);

		//thuc hien truy van du lieu
		$query = $this->db->get($this->table);
		//echo $this->db->last_query();
		return $query->result();
	}

	/**
	 * Gan cac thuoc tinh trong input khi lay danh sach
	 * $input : mang du lieu dau vao
	 */

	protected function get_list_set_input($input = array())
	{
		// Thêm điều kiện cho câu truy vấn truyền qua biến $input['where']
		//(vi du: $input['where'] = array('email' => 'hocphp@gmail.com'))
		if ((isset($input['where'])) && $input['where'])
		{
			$this->db->where($input['where']);
		}
	}

	/**
	 * kiểm tra sự tồn tại của dữ liệu theo 1 điều kiện nào đó
	 * $where : mang du lieu dieu kien
	 */
    function check_exists($where = array())
    {
	    $this->db->where($where);
	    //thuc hien cau truy van lay du lieu
		$query = $this->db->get($this->table);

		if($query->num_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	// Data: array("param" => "value") ==> index.php?param=value

    function callAPI($method, $url, $data = false) {
        $curl = curl_init();

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        //curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
	}

	function utf8convert($str) {

		if(!$str) return false;

		$utf8 = array(

			'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

			'd'=>'đ|Đ',

			'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

			'i'=>'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',

			'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

			'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

			'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',

		);

		foreach($utf8 as $ascii=>$uni) $str = preg_replace("/($uni)/i",$ascii,$str);

		return $str;

	}

	function utf8tourl($text){
        $text = strtolower($this->utf8convert($text));
        $text = str_replace( "ß", "ss", $text);
        $text = str_replace( "%", "", $text);
        $text = preg_replace("/[^_a-zA-Z0-9 -] /", "",$text);
        $text = str_replace(array('%20', ' '), '-', $text);
        $text = str_replace("----","-",$text);
        $text = str_replace("---","-",$text);
		$text = str_replace("--","-",$text);
		return $text;
	}
	function sluggify($url)
	{
		# Prep string with some basic normalization
		$url = strtolower($this->utf8convert($url));
		$url = strip_tags($url);
		$url = stripslashes($url);
		$url = html_entity_decode($url);

		# Remove quotes (can't, etc.)
		$url = str_replace('\'', '', $url);

		# Replace non-alpha numeric with hyphens
		$match = '/[^a-z0-9]+/';
		$replace = '-';
		$url = preg_replace($match, $replace, $url);

		$url = trim($url, '-');

		return $url;
	}
  
	public function resizeImage($filename) {
		require_once APPPATH . '/libraries/ThumbLib.inc.php';
		$thumb 		= PhpThumbFactory::create($filename);
		$thumb->resize(350, 350);
		$thumb->save($filename);
	}

	public function get_seo_data($where) {
		$this->db->select("image,seo_description,seo_keywords");
		$this->db->from($this->table);
		$this->db->where($where);
		return $this->db->get()->row_array();
	}
}
