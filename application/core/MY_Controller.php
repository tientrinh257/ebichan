<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('MY_model');
		$this->check_login();
    }

    private function check_login() {
		
    	//lay ra gia tri cua controller
    	$module = $this->uri->segment(1);
    	$module = strtolower($module);

		if ( $module == "user" )
			return;
    	$login = $this->session->userdata('login');
    	// neu admin chua dang nhap thi chi cho vao trang login
    	// neu chua ton tai session login va controller khong phai login

    	if(!$login && $module == 'admin')
    	{
    		redirect(auth_url());
    	}
    	if($login && $module == 'authentication')
    	{
    		redirect(admin_url('Admin'));
    	}
    }

}