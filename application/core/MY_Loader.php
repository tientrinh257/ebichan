<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Loader.php";

class MY_Loader extends MX_Loader {
    function __construct() {
        parent::__construct();
        if (version_compare(CI_VERSION, '3.1.2', '<')) {
            $this->load->library('security');
        }
    }
}
