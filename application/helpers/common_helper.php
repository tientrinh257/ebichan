<?php
	function public_url($string = '') {
		return base_url('public/'.$string);
	}

	function admin_url($string = '') {
		return base_url('admin/'.$string);
	}

	function user_url($string = '') {
		return base_url('user/'.$string);
	}

	function auth_url($string = '') {
		return base_url('authentication/'.$string);
	}
?>
