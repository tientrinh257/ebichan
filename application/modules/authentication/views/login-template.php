<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset=utf-8 />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
 	<title>AcomAgencyVN | Login</title>
	<link rel="stylesheet" href="<?php echo public_url('css/common/bootstrap.min.css') ?>" />
	<script src="<?php echo public_url('js/common/jquery-3.5.1.min.js')?>"></script>
	<script src="<?php echo public_url('js/common/bootstrap.bundle.min.js')?>"></script>

</head>
<style>
.col-md-4{
	margin-top: 8%;
}
.btn{
	width:100%;
	margin-top: 15px;
}
</style>
<body>
<div class="container text-center" style="border:none">
	<div class="row">
		<div class="col-md-4 offset-md-4">
			<img class="mb-4" src="<?= base_url('logo.png') ?>" alt="" height="72">
			<h3 style="margin-bottom: 20px;">Đăng nhập quản trị </h3>
			<span style="color:red"><?php echo $this->session->flashdata('login_state');?></span>
			<form method="post">
				<div class="form-group">
					<input name="user_name" type="text" value="<?php echo set_value('user_name')?>" class="form-control" placeholder="Tên đăng nhập" required>
				</div>
				<div class="form-group">
					<input name="password" type="password" value="" class="form-control" placeholder="Mật khẩu" required>
				</div>
				<input type="submit" value="Đăng nhập" class="btn btn-primary">
			</form>
		</div>
	</div>
</div>
</body>
<html>
