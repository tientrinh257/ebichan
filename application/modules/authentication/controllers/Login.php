<?php
class Login extends MY_Controller
{
    function __construct()
    {
     	parent::__construct();
     	$this->load->model('Login_model');
    }
    function check_authenication()
    {
    	$taikhoan = $this->input->post('user_name');
    	$matkhau = md5($this->input->post('password'));
    	$where = array('user_name'=>$taikhoan,'password'=>$matkhau);
    	if($this->Login_model->check_exists($where))
    	{
    		return true;
    	}
    	else
    	{
    		// tao 1 message thong bao dang nhap ko thanh cong
    		$this->session->set_flashdata('login_state','Tên đăng nhập hoặc mật khẩu không đúng!');
    		return false;
    	}
    }

    function index()
    {
    	if($this->input->post())
    	{
    		//goi den ham kiem tra dang nhap check_login
    		$this->form_validation->set_rules('login','login','callback_check_authenication');
    		if($this->form_validation->run())
    		{
    			$taikhoan = $this->input->post('user_name');
    			//neu form da chay dung thi se tao 1 session cho admin
				$this->session->set_userdata('login',$taikhoan);
				if($_SERVER['HTTP_REFERER']) {
					redirect($_SERVER['HTTP_REFERER']);
				} else {
					redirect(admin_url('Admin'));
				}
    			
    		}
    	}
    	$this->load->view('authentication/login-template');
    }
}