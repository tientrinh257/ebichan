<?php
class Login_model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->table = 'account';
	}

	function get_user($id) {
		$this->db->where('id', $id);
		$result = $this->db->get($this->table);
		return $result->row_array();
	}
}
?>
