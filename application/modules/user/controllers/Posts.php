<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class Posts extends MY_Controller {
    function __construct() {
        parent::__construct();
		$this->load->model("admin/Posts_model");
		$this->load->library('pagination');
    }

    function index() {
		$data["menu"] = "bai-viet";
		$data["page_view"] = "post_list";
		$data["list"] = $this->Posts_model->get_posts_list(0,12);
		$data["side_posts_list"] = $this->Posts_model->get_side_post_list();
		$data['page_offset'] = $this->uri->segment(3);
		$data["title"] = "Bài viết";
		$this->load->view('index',$data);
	}

	function partial_list() {
        if($this->input->post()) {
            $page = empty($this->input->post("page_offset")) ? 0 : $this->input->post("page_offset");
            $config["cur_page"] = $page;

            $limit=12;
            $offset= $limit * ($page - 1) ;
            if($offset < 0) $offset = 0;
            
            $data_per_page = $this->Posts_model->get_limit_posts($limit,$offset);

            $all = $this->Posts_model->get_all_posts();
            
            $config['total_rows'] = $all;
            $config['per_page'] = $limit;
			$config['base_url'] = base_url('bai-viet/trang');
            $this->pagination->initialize($config);
            $paginator=$this->pagination->create_links();
            $data['paginator'] = $paginator;
            $data['list'] = $data_per_page;
            $this->load->view('partial_list',$data);
        }
	}

	function get_posts($url) {
		$data["menu"] = "bai-viet";
		$data["page_view"] = "post";
		$data["detail"] = $this->Posts_model->get_posts_by_name($url);
		$data["seo"] = $this->Posts_model->get_seo_data(array("url"=>$url));
		$data["side_posts_list"] = $this->Posts_model->get_side_post_list();
		$data["title"] = $data["detail"]["title"];
		$this->load->view('index',$data);
	}

	function load_more() {
		if( $this->input->post() ) {
			$offset = $this->input->post("offset");
			$result = $this->Posts_model->get_posts_list($offset);
			echo json_encode($result);
		}
	}

	function side_load_more() {
		if( $this->input->post() ) {
			$offset = $this->input->post("offset");
			$result = $this->Posts_model->get_side_post_list($offset);
			echo json_encode($result);
		}
	}

	function author($author) {
		$author = urldecode($author);
		$data["menu"] = "bai-viet";
		$data["page_view"] = "post_list";
		$data["list"] = $this->Posts_model->get_posts_list_author($author);
		$data["seo"] = $this->Posts_model->get_seo_data(array("author"=>$author));
		$data["title"] = "Bài viết";
		$this->load->view('index',$data);
	}
}