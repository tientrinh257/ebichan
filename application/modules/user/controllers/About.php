<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class About extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/About_model');
	}

	function index() {
		$data["menu"] = "ve-chung-toi";
		$data["page_view"] = "about";
		$data["detail"] = $this->About_model->get_content();
		$data["seo"] = $this->About_model->get_seo_data(array("id"=>1));
		$data["title"] = "Về chúng tôi";
        $this->load->view('index',$data);
	}

}
