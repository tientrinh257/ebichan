<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class Services extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model("admin/Services_model");
    }

	function services($url) {
		$data["menu"] = "dich-vu";
		$data["page_view"] = "services";
		$data["detail"] = $this->Services_model->get_by_name($url);
		$data["seo"] = $this->Services_model->get_seo_data(array("url"=>$url));
		$data["list_services"] = $this->Services_model->list_services();
		$data["title"] = $data["detail"]["name"];
		$this->load->view('index',$data);
	}
}