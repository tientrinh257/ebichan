<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User_C extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model(array('User_model'));
	}

    function index() {
		$this->load->model(array('admin/Product_model','admin/Video_model', 'admin/Home_model'));
		$data["menu"] = "trang-chu";
		$data['page_view'] = 'home';
		$data["seo"] = $this->Home_model->get_content();
		$data["list_product"] = $this->Product_model->get_all_product_list();
		$data["list_video"] = $this->Video_model->get_all_video_list();
		$data["title"] = "Trang chủ";
        $this->load->view('user/index',$data);
	}

	function amp() {
		$this->load->model(array('admin/Home_model'));
		$data["menu"] = "trang-chu";
		$data['page_view'] = 'home';
		$data["seo"] = $this->Home_model->get_content();
        $this->load->view('user/amp',$data);
	}

	function send_message() {
        if($this->input->post()) {
			$this->load->model('Message_model');
            $post_data = $this->input->post();
			$result = array();
			if($this->Message_model->create($post_data)) {
				$result["message"] = "Tin nhắn của bạn đã đươc gửi. Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất";
			} else {
				$result["message"] = "Có lỗi xảy ra khi gửi tin nhắn. Vui lòng tải lại trang và thử lại";
			}
			echo json_encode($result);
        }
    }
}
