<?php 
  $image = !empty($seo["image"]) ? base_url($seo["image"]) : "";
	$description = !empty($seo["seo_description"]) ? $seo["seo_description"] : "";
	$keywords = !empty($seo["seo_keywords"]) ? $seo["seo_keywords"] : "";
	$title = !empty($title) ? $title : "Ebichan";
?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
<meta property="og:locale" content="vi_VN">
<meta property="og:type" content="website">
<meta property="og:title" content="<?= $title ?>">
<meta property="og:url" content="<?= current_url() ?>">
<meta property="og:site_name" content="Ebichan">
<meta property="og:image" content="<?= $image ?>">
<meta property="og:description" content="<?= $description ?>">
<meta name="description" content="<?= $description ?>">
<meta name="keywords" content="<?= $keywords ?>">
<meta name="robots" content="index, follow">
<meta name="author" content="ebichan">
<?php if ($page_view == "home") { ?>
  <link rel="amphtml" href="<?= base_url('amp') ?>">
<?php } ?>
<link rel="canonical" href="<?= current_url() ?>" >
<title><?= $title ?> | Ebichan</title>

<link rel="stylesheet" href="<?= public_url('css/common/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?= public_url('css/user/main.css').'?'. JS_FILE_VERSION ?>">
<link rel="stylesheet" href="<?= public_url('css/user/responsive.css').'?'. JS_FILE_VERSION ?>">
<link rel="stylesheet" href="<?= public_url('slick/slick.css') ?>">
<link rel="stylesheet" href="<?= public_url('slick/slick-theme.css') ?>">
<script src="<?= public_url('js/common/jquery-3.5.1.min.js') ?>"></script>