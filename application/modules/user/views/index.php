<!DOCTYPE html>
<html lang="vn">
    <head>
        <?php $this->load->view("head",$seo = array()) ?>
    </head>
    <body>
        <div id="main-wrap">      
            <?php $this->load->view("menu") ?>
            <?php $this->load->view($page_view) ?>
            <?php $this->load->view("footer") ?>
            <?php $this->load->view("modal"); ?>
            <input type="hidden" id="current_menu" value="<?= $menu ?>" />
            <input type="hidden" id="base_url" value="<?= base_url() ?>" />
        </div>
    </body>
</html>