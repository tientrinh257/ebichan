<footer>
    <div id="contact" class="container pt-5">
        <div class="row pt-5">
            <div class="col-lg-12">
                <h2 class="red-text">Contact Us</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <div class="red-line"></div>
                <div class="red-line"></div>
            </div>
            <div class="col-lg-12">
                <div class="red-line"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="red-text bold-text pt-5">Ebi-chan</h4>
                <p>Loren ipsum Loren ipsum Loren ipsum Loren ipsum</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-4 col-sm-6 pd-0 gmail-info pt-3">
                        <img class="img-fluid" alt="mail" src="<?= public_url('css/user/assets/mail.png') ?>" />
                        <span class="pl-2">support@ebi-chan.vn</span>
                    </div>
                    <div class="col-lg-4 col-sm-6 pd-0 fb-info pt-3">
                        <a href="https://www.facebook.com/Ebichan.Offical/" target="_blank" class="link">
                            <img class="img-fluid" alt="mail" src="<?= public_url('css/user/assets/facebook.png') ?>" />
                            <span class="pl-2">fb.com/Ebichan.Offical</span>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6 pd-0 youtube-info pt-3">
                        <a href="https://Youtu.be/Ebichan.Offical" target="_blank" class="link">
                            <img class="img-fluid" alt="mail" src="<?= public_url('css/user/assets/youtube.png') ?>" />
                            <span class="pl-2">Youtu.be/Ebichan.Offical</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 pt-5">
                <div class="bottom-logo">
                    <img class="img-fluid" alt="logo" src="<?= public_url('css/user/assets/logo.png') ?>" />
                </div>
            </div>
            <div class="col-sm-8 align-self-end ">
                <div class="text-right">2020 Ebichan Website Copyright. <br class="d-md-none">All Rights Reserved.</div>
            </div>
        </div>
    </div>
</footer>
<div id="btn_move_top">
    <img class="img-fluid" alt="clock" src="<?= base_url('public/css/user/assets/chevron-up-solid.svg') ?>" />
</div>
<script src="<?= public_url('js/common/bootstrap.bundle.min.js') ?>"></script>
<script src="<?= public_url('js/user/menu.js').'?'. JS_FILE_VERSION ?>"></script>
<script src="<?= public_url('slick/slick.min.js') ?>"></script>
<script src="<?= public_url('js//user/home.js').'?'. JS_FILE_VERSION ?>"></script>