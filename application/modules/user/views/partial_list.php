<div class="row">
	<?php foreach($list as $key => $value) : ?>
		<article class="col-md-4">
			<div class="">
				<a href="<?= base_url('bai-viet/') .$value['url'] ?>" title="<?= $value["title"] ?>">
					<img class="width-100" width="300" height="200" src="<?= base_url($value["image"]) ?>">
				</a>
			</div>
			<div class="posts-topic">
				<h2 class="posts-title h5"><a href="<?= base_url('bai-viet/') .$value['url'] ?>"><?= $value["title"] ?></a></h2>
				<p class="text-muted small mt-1"><span class="clock"><img class="img-fluid" alt="clock" src="<?= base_url('public/css/user/assets/clock-regular.svg') ?>" width="12px" height="12px" ></span> <?= strftime("%d-%m-%Y",strtotime($value["create_datetime"])) ?></p>
			</div>
		</article>
	<?php endforeach; ?>
</div>
<?= $paginator ?>