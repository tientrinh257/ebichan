<link href="<?= public_url('css/user/post.css').'?'. JS_FILE_VERSION ?>" rel="stylesheet">
<link href="<?= public_url('ckeditor5/ckeditor.css').'?'. JS_FILE_VERSION ?>" rel="stylesheet">

<main role="main" class="container">
	<div class="row">
		<div class="col-md-9 news-main mt-5">
			<div class="news-post">
				<h2 class="news-post-title"><?= $detail["title"] ?></h2>
				<div class="news-content ck-content">
					<br>
					<?= $detail["content"] ?>
				</div>
			</div><!-- /.news-post -->

		</div><!-- /.news-main -->

		<aside id="right" class="col-md-3 mt-5 sticky-top">
			<div class="posts-sidebar">
				<h4 class="widget-title h6"><span>BÀI VIẾT MỚI</span></h4>
				<ol class="list-unstyled" style="padding: 0 15px;">
					<?php foreach ($side_posts_list as $key => $value) { ?>
						<li class="posts-topic">
							<a href="<?= base_url('bai-viet/') .$value['url'] ?>"><?= $value['title'] ?></a>
						</li>
					<?php } ?>
				</ol>
			</div>
		</aside><!-- /.posts-sidebar -->

	</div><!-- /.row -->

</main><!-- /.container -->
<script src="<?= public_url('js/user/news.js').'?'. JS_FILE_VERSION ?>"></script>