<link rel="stylesheet" href="<?= public_url('css/user/post.css').'?'. JS_FILE_VERSION ?>">
<input type="hidden" id="page_offset" page_offset="<?php echo $page_offset ?>" />
<main role="main" class="container">
	<div class="row">
		<div class="col-md-9 mt-5" id="posts-list">
			<!-- Example row of columns -->
		</div> <!-- /container -->
		<aside id="right" class="col-md-3 mt-5 sticky-top">
			<div class="posts-sidebar">
				<h4 class="widget-title h6"><span>BÀI VIẾT MỚI</span></h4>
				<ol class="list-unstyled" style="padding: 0 15px;">
					<?php foreach ($side_posts_list as $key => $value) { ?>
						<li class="posts-topic">
							<a href="<?= base_url('bai-viet/') .$value['url'] ?>"><?= $value['title'] ?></a>
						</li>
					<?php } ?>
				</ol>
			</div>
		</aside><!-- /.posts-sidebar -->
	</div>
</main>
<script src="<?= public_url('js/user/posts_list.js').'?'. JS_FILE_VERSION ?>"></script>