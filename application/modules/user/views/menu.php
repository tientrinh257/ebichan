<div id="main-menu" class="fixed-top">
    <div class="menu-wrap">
        <nav id="navbar" class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <a class="navbar-brand" href="<?= base_url() ?>">
                    <img class="img-fluid" alt="logo" src="<?= public_url('css/user/assets/main-logo.png') ?>" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="menu">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item" data-name="story">
                            <a class="nav-link active" href="<?= base_url() ?>">Story</a>
                        </li>
                        <li class="nav-item" data-name="our-product">
                            <a class="nav-link" href="<?= base_url() ?>">Our Product</a>
                        </li>
                        <li class="nav-item" data-name="mini-games">
                            <a class="nav-link" href="<?= base_url() ?>">Mini Games</a>
                        </li>
                        <li class="nav-item" data-name="contact-us">
                            <a class="nav-link" href="#contact-us">Contact Us</a>
                        </li>
                        <li class="nav-item" data-name="member-ship">
                            <a class="nav-link" href="#members-ship">Members Ship</a>
                        </li>
                        <li class="nav-item" data-name="workshop">
                            <a class="nav-link" href="#workshop">Workshop</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>