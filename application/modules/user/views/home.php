<link rel="stylesheet" href="<?= public_url('css/user/home.css').'?'. JS_FILE_VERSION ?>">
<!-- <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet"> -->
<div class="home text-center">
	<div class="row content-block block-1">
		<div class="col-12 pd-0">
			<img class="img-fluid" alt="about-cover" src="<?= public_url('css/user/assets/home-top.png') ?>" />
		</div>
		<div class="col-6 mini-game">
			<img class="img-fluid" alt="about-cover" src="<?= public_url('css/user/assets/mini-game.png') ?>" />
		</div>
	</div>
	<div class="row content-block block-2 pb-5">
		<div class="col-6 align-self-center">
			<img class="img-fluid" alt="about-cover" src="<?= public_url('css/user/assets/mini-game-prize.png') ?>" />
		</div>
		<div class="col-6">
			<img class="img-fluid" alt="about-cover" src="<?= public_url('css/user/assets/mini-game-list.png') ?>" />
		</div>
	</div>
	<div class="workshop-wrap">
		<div class="row content-block block-3 pt-5">
			<div class="col-12 pb-5">
				<img class="img-fluid" alt="about-cover" src="<?= public_url('css/user/assets/the-workshop.png') ?>" />
			</div>
			<div class="row col-12 workshop-item">
				<div class="col-4 pt-5 circle-1 circle" data-aos="fade-up-right" data-aos-easing="linear" data-aos-duration="100">
					<a href=""><img class="img-fluid" alt="about-cover" src="<?= public_url('css/user/assets/green.png') ?>" /></a>
				</div>
				<div class="col-4 pt-5 circle-2 circle" data-aos="fade-up-right" data-aos-easing="linear" data-aos-duration="100">
					<a href=""><img class="img-fluid" alt="about-cover" src="<?= public_url('css/user/assets/workshop-1.png') ?>" /></a>
				</div>
				<div class="col-4 pt-5 circle-3 circle" data-aos="fade-up-right" data-aos-easing="linear" data-aos-duration="100">
					<a href=""><img class="img-fluid" alt="about-cover" src="<?= public_url('css/user/assets/blue.png') ?>" /></a>
				</div>
			</div>
		</div>
		<div class="row content-block block-4">
			<div class="row col-12 workshop-item pt-5 pb-5">
				<div class="col-4 circle-4 circle" data-aos="fade-up-right" data-aos-easing="linear" data-aos-duration="100">
					<a href=""><img class="img-fluid" alt="about-cover" src="<?= public_url('css/user/assets/workshop-2.png') ?>" /></a>
				</div>
				<div class="col-4 circle-5 circle" data-aos="fade-up-right" data-aos-easing="linear" data-aos-duration="100">
					<a href=""><img class="img-fluid" alt="about-cover" src="<?= public_url('css/user/assets/red.png') ?>" /></a>
				</div>
				<div class="col-4 circle-6 circle" data-aos="fade-up-right" data-aos-easing="linear" data-aos-duration="100">
					<a href=""><img class="img-fluid" alt="about-cover" src="<?= public_url('css/user/assets/workshop-3.png') ?>" /></a>
				</div>
			</div>
			<div class="col-12 pd-0">
				<img class="img-fluid" alt="about-cover" src="<?= public_url('css/user/assets/home-bottom.png') ?>" />
			</div>
		</div>
	</div>
</div>
<!-- <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script> -->