<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Posts extends MY_Controller {
	function __construct() {
		parent::__construct();

        $this->load->model(array('Posts_model'));
        $this->data["page_view"] = "posts";
    }

    public function list() {
        $this->data["temp"] = "posts/list";
        // get posts list
        $this->data['posts_list'] = $this->Posts_model->get_all_posts_list();

        // load view
        
        $this->load->view("admin/index",$this->data);
    }

    function search() {
        $this->data["temp"] = "posts/list";

        // get search string
        $search_title = ($this->input->post("posts_title"))? $this->input->post("posts_title") : "NULL";

        $search_title = ($this->uri->segment(4)) ? $this->uri->segment(4) : $search_title;

        $this->data['posts_list'] = $this->Posts_model->get_all_posts_list($search_title);

        //Load view
        $this->load->view('admin/index',$this->data);
    }

    function create() {
        $this->data["temp"] = "posts/create";
        $this->load->view("admin/index",$this->data);
    }

    function edit($id) {
        $this->data["temp"] = "posts/edit";
        
        $this->data["posts_detail"] = $this->Posts_model->get_posts($id);
        $this->load->view("admin/index",$this->data);
    }

    function insert() {
        $post_data = $this->input->post();
        if ( $post_data ) {

            $res = array();
            if(!$this->Posts_model->check_exists(array('title'=>$post_data["title"]))) {
                $target_dir = "assets/posts/";
                if (!file_exists($target_dir)) {
                    mkdir($target_dir, 0777, true);
                }
                $thumb_img = $target_dir . basename($_FILES["thumb_img"]["name"]);
                
                if( !file_exists($thumb_img) ) move_uploaded_file($_FILES["thumb_img"]["tmp_name"], $thumb_img);
                $this->Posts_model->resizeImage($thumb_img);
                
                $insert_data = array(
                    "title"         => $post_data["title"],
                    "url"           => $this->Posts_model->sluggify($post_data["title"]),
                    "content"       => $this->input->post("content"),
                    "image"         => $thumb_img,
                    'seo_description'   => $post_data['description'],
                    'seo_keywords'      => $post_data['keywords']
                );

                $this->Posts_model->create($insert_data);
                $res['code'] = 200;
                $res['message'] = "add successful";
            } else {
                $res["code"] = 201;
                $res['message'] = "posts title already exists";
            }
            echo json_encode($res);
        }
    }

    function update() {
        $post_data = $this->input->post();
        if ( $post_data ) {

            $thumb_img = "";

            if( $_FILES ) {
                $target_dir = "assets/posts/";
                if( isset( $_FILES["thumb_img"] ) ) {
                    $thumb_img = $target_dir . basename($_FILES["thumb_img"]["name"]);
                    if( !file_exists($thumb_img) )
                        move_uploaded_file($_FILES["thumb_img"]["tmp_name"], $thumb_img);
                    $this->Posts_model->resizeImage($thumb_img);
                }
                
            }

            $res = array();
            $update_data = array(
                'title'         => $post_data['title'],
                "url"           => $this->Posts_model->sluggify($post_data["title"]),
                'content'       => $post_data['content'],
                'seo_description'   => $post_data['description'],
                'seo_keywords'      => $post_data['keywords']
            );

            if( !empty($thumb_img) ) {
                $update_data["image"] = $thumb_img;
            }

            $this->Posts_model->update_rule(array('id'=>$post_data['posts_id']),$update_data);
            $res['code'] = 200;
            $res['message'] = "update successful";
            echo json_encode($res);
        }
    }

    function delete($id) {
        $this->Posts_model->delete($id);
        redirect(base_url('admin/posts/list'));
    }

}
