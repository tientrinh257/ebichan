<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product extends MY_Controller {
	function __construct() {
		parent::__construct();

        $this->load->model(array('product_model'));
        $this->data["page_view"] = "product";
    }

    public function list() {
        $this->data["temp"] = "product/list";
        // get product list
        $this->data['product_list'] = $this->product_model->get_all_product_list();

        // load view
        
        $this->load->view("admin/index",$this->data);
    }

    function search() {
        $this->data["temp"] = "product/list";

        // get search string
        $search_title = ($this->input->post("product_title"))? $this->input->post("product_title") : "NULL";

        $search_title = ($this->uri->segment(4)) ? $this->uri->segment(4) : $search_title;

        $this->data['product_list'] = $this->product_model->get_all_product_list($search_title);

        //Load view
        $this->load->view('admin/index',$this->data);
    }

    function create() {
        $this->data["temp"] = "product/create";
        $this->load->view("admin/index",$this->data);
    }

    function edit($id) {
        $this->data["temp"] = "product/edit";
        
        $this->data["product_detail"] = $this->product_model->get_product($id);
        $this->load->view("admin/index",$this->data);
    }

    function insert() {
        $post_data = $this->input->post();
        if ( $post_data ) {

            $res = array();
            if(!$this->product_model->check_exists(array('title'=>$post_data["title"]))) {
                $target_dir = "assets/product/";
                if (!file_exists($target_dir)) {
                    mkdir($target_dir, 0777, true);
                }
                $thumb_img = $target_dir . basename($_FILES["thumb_img"]["name"]);
    
                $data["message"] = array();
                
                if( !file_exists($thumb_img) ) move_uploaded_file($_FILES["thumb_img"]["tmp_name"], $thumb_img);
                $this->product_model->resizeImage($thumb_img);
                
                $insert_data = array(
                    "title"         => $post_data["title"],
                    "url"           => $this->product_model->sluggify($post_data["title"]),
                    // "content"       => $this->input->post("content"),
                    "image"         => $thumb_img,
                );

                $this->product_model->create($insert_data);
                $res['code'] = 200;
                $res['message'] = "add successful";
            } else {
                $res["code"] = 201;
                $res['message'] = "product title already exists";
            }
            echo json_encode($res);
        }
    }

    function update() {
        $post_data = $this->input->post();
        if ( $post_data ) {

            $data["message"] = array();
            $thumb_img = "";

            if( $_FILES ) {
                $target_dir = "assets/product/";
                if( isset( $_FILES["thumb_img"] ) ) {
                    $thumb_img = $target_dir . basename($_FILES["thumb_img"]["name"]);
                    if( !file_exists($thumb_img) )
                        move_uploaded_file($_FILES["thumb_img"]["tmp_name"], $thumb_img);
                    $this->product_model->resizeImage($thumb_img);
                }
                
            }

            $res = array();
            $update_data = array(
                'title'         => $post_data['title'],
                "url"           => $this->product_model->sluggify($post_data["title"]),
                // 'content'       => $post_data['content'],
            );

            if( !empty($thumb_img) ) {
                $update_data["image"] = $thumb_img;
            }

            $this->product_model->update_rule(array('id'=>$post_data['product_id']),$update_data);
            $res['code'] = 200;
            $res['message'] = "update successful";
            echo json_encode($res);
        }
    }

    function delete($id) {
        $this->product_model->delete($id);
        redirect(base_url('admin/product/list'));
    }

}
