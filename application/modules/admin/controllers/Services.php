<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Services extends MY_Controller {
	function __construct() {
		parent::__construct();

        $this->load->model(array('Services_model'));
        $this->data["page_view"] = "services";
    }

    public function list() {
        $this->data["temp"] = "services/list";

        // get services list
        $this->data['services_list'] = $this->Services_model->get_services();

        // load view
        
        $this->load->view("admin/index",$this->data);
    }

    function edit($id) {
        $this->data["temp"] = "services/edit";
        
        $this->data["service_detail"] = $this->Services_model->get_service($id);
        $this->load->view("admin/index",$this->data);
    }

    function update() {
        $post_data = $this->input->post();
        if ( $post_data ) {
            
            $thumb_img = "";
            if( $_FILES ) {
                $target_dir = "assets/services/";
                if( isset( $_FILES["thumb_img"] ) ) {
                    $thumb_img = $target_dir . basename($_FILES["thumb_img"]["name"]);
                    if( !file_exists($thumb_img) )
                        move_uploaded_file($_FILES["thumb_img"]["tmp_name"], $thumb_img);
                    $this->Services_model->resizeImage($thumb_img);
                }
                
            }

            $res = array();
            $update_data = array(
                'name' => $post_data['name'],
                'content' => $post_data['content'],
                'seo_description'   => $post_data['description'],
                'seo_keywords'      => $post_data['keywords'],
            );

            if( !empty($thumb_img) ) {
                $update_data["image"] = $thumb_img;
            }

            $this->Services_model->update_rule(array('id'=>$post_data['service_id']),$update_data);
            $res['code'] = 200;
            $res['message'] = "update successful";
            echo json_encode($res);
        }
    }

    function delete($id) {
        $this->Services_model->delete($id);
        redirect(base_url('admin/services'));
    }

}
