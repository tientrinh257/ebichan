<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Account extends MY_Controller
{
	function __construct() {
		parent::__construct();

		$this->load->model(array('authentication/Login_model'));
	}

	function list() {
		$data['temp'] = 'account/list';
		$data["page_view"] = "account";
		$data['list'] = $this->db->get('account')->result_array();
		$this->load->view('admin/index',$data);
	}

	function edit($id) {
		$data = array();
        $info = $this->Login_model->get_user($id);

		$data["info"] = $info;
		$data["page_view"] = "account";
        $data['temp'] = 'account/edit';
        $this->load->view('admin/index',$data);

	}

	function update() {
        $post_data = $this->input->post();
        if ( $post_data ) {

            $data["message"] = array();

            $res = array();
            $update_data = array(
                'password' => md5($post_data['user_password']),
            );

            $this->Login_model->update_rule(array('id'=>$post_data['user_id']),$update_data);
            $res['code'] = 200;
            $res['message'] = "update successful";
            echo json_encode($res);
        }
    }

}