<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Video extends MY_Controller {
	function __construct() {
		parent::__construct();

        $this->load->model(array('Video_model'));
        $this->data["page_view"] = "video";
    }

    public function list() {
        $this->data["temp"] = "video/list";
        // get video list
        $this->data['video_list'] = $this->Video_model->get_all_video_list();

        // load view
        
        $this->load->view("admin/index",$this->data);
    }

    function search() {
        $this->data["temp"] = "video/list";

        // get search string
        $search_title = ($this->input->post("video-title"))? $this->input->post("video-title") : "NULL";

        $search_title = ($this->uri->segment(4)) ? $this->uri->segment(4) : $search_title;

        $this->data['video_list'] = $this->video_model->get_all_video_list($search_title);

        //Load view
        $this->load->view('admin/index',$this->data);
    }

    function create() {
        $this->data["temp"] = "video/create";
        $this->load->view("admin/index",$this->data);
    }

    function edit($id) {
        $this->data["temp"] = "video/edit";
        
        $this->data["video_detail"] = $this->Video_model->get_video($id);
        $this->load->view("admin/index",$this->data);
    }

    function insert() {
        $post_data = $this->input->post();
        if ( $post_data ) {

            $res = array();
            if(!$this->Video_model->check_exists(array('title'=>$post_data["title"]))) {
                $target_dir = "assets/video/";
                if (!file_exists($target_dir)) {
                    mkdir($target_dir, 0777, true);
                }
                $thumb_img = $target_dir . basename($_FILES["thumb-img"]["name"]);
    
                $data["message"] = array();
                
                if( !file_exists($thumb_img) ) move_uploaded_file($_FILES["thumb-img"]["tmp_name"], $thumb_img);
                $this->Video_model->resizeImage($thumb_img);
                
                $insert_data = array(
                    "title"         => $post_data["title"],
                    "link_video"    => $post_data["link-video"],
                    "image"         => $thumb_img,
                );

                $this->Video_model->create($insert_data);
                $res['code'] = 200;
                $res['message'] = "add successful";
            } else {
                $res["code"] = 201;
                $res['message'] = "Video title already exists";
            }
            echo json_encode($res);
        }
    }

    function update() {
        $post_data = $this->input->post();
        if ( $post_data ) {

            $data["message"] = array();
            $thumb_img = "";

            if( $_FILES ) {
                $target_dir = "assets/video/";
                if( isset( $_FILES["thumb-img"] ) ) {
                    $thumb_img = $target_dir . basename($_FILES["thumb-img"]["name"]);
                    if( !file_exists($thumb_img) )
                        move_uploaded_file($_FILES["thumb_img"]["tmp-name"], $thumb_img);
                    $this->Video_model->resizeImage($thumb_img);
                }
                
            }

            $res = array();
            $update_data = array(
                'title'         => $post_data['title'],
                "link_video"    => $post_data['link-video'],
            );

            if( !empty($thumb_img) ) {
                $update_data["image"] = $thumb_img;
            }

            $this->Video_model->update_rule(array('id'=>$post_data['video-id']),$update_data);
            $res['code'] = 200;
            $res['message'] = "update successful";
            echo json_encode($res);
        }
    }

    function delete($id) {
        $this->Video_model->delete($id);
        redirect(base_url('admin/video/list'));
    }

}
