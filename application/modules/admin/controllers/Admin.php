<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends MY_Controller
{
	function __construct() {
		parent::__construct();

		$this->load->model(array('authentication/Login_model',"Admin_model","Message_model"));
	}

	function index() {
		if($this->session->userdata('login')) {
			$data["temp"] = "dashboard";
			$data["page_view"] = "dashboard";
			$data["count_message"] = $this->Message_model->get_message_count("New");

			$this->load->view('index',$data);
		} else {
			redirect(auth_url('Login'));
		}
	}

	function logout() {
		if($this->session->userdata('login'))
		{
			$this->session->unset_userdata('login');
			redirect(auth_url('Login'));
		}
	}

}