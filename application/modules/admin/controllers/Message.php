<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Message extends MY_Controller {
    function __construct() {
        parent::__construct();

        $this->load->model("Message_model");
        
    }

    public function show() {
        $data["temp"] = "message/show";
        $data["page_view"] = "messages";

        //pagination settings
        $config['base_url'] = admin_url('Message/show');
        $config['total_rows'] = $this->db->where("disable",0)->count_all('messages');
        $config['per_page'] = 25;
        $config["uri_segment"] = 4;

        $choice = $config["total_rows"]/$config["per_page"];
        $config["num_links"] = floor($choice);

        // integrate bootstrap pagination
        
        $this->pagination->initialize($config);

        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$data["offset"]= $config['per_page'] * ($data['page'] - 1) ;
		if($data["offset"] < 0) $data["offset"] = 0;

        // get message list
        $data['message_list'] = $this->Message_model->get_message_list($config['per_page'], $data["offset"], "NULL", "DESC");

        $data['pagination'] = $this->pagination->create_links();

        // load view
        
        $this->load->view("index",$data);
    }

    function search() {
        $data["temp"] = "message/show";
        $data["page_view"] = "messages";

        // get search string
        $search_status = $this->input->post("message_status") != null ? $this->input->post("message_status") : "NULL";

        $search_status = ($this->uri->segment(4)) ? $this->uri->segment(4) : $search_status;

        $search_date = ($this->input->post("date_sort"))? $this->input->post("date_sort") : "NULL";

        $search_date = ($this->uri->segment(5)) ? $this->uri->segment(5) : $search_date;

        // pagination settings
        $config = array();
        $config['base_url'] = admin_url("Message/search/$search_status/$search_date");
        $config['total_rows'] = $this->Message_model->get_message_count($search_status);
        $config['per_page'] = "25";
        $config["uri_segment"] = 6;
        $choice = $config["total_rows"]/$config["per_page"];
        $config["num_links"] = floor($choice);

        // integrate bootstrap pagination
        $this->pagination->initialize($config);

        $data['page'] = ($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
        $data["offset"]= $config['per_page'] * ($data['page'] - 1) ;
		if($data["offset"] < 0) $data["offset"] = 0;
        // get message list
        $data['message_list'] = $this->Message_model->get_message_list($config['per_page'], $data["offset"], $search_status, $search_date);

        $data['pagination'] = $this->pagination->create_links();

        //Load view
        $this->load->view('index',$data);
    }

    function view($id) {
        $data["temp"] = "message/view";
        $data["page_view"] = "dashboard";

        $data['message_detail'] = $this->Message_model->get_message($id);

        if($data["message_detail"]["viewed"] == 0 )
            $this->Message_model->update_status($id);

        $this->load->view("index",$data);
    }

    function delete($id) {
        $this->Message_model->delete($id);
        redirect(base_url('admin/Message/show'));
    }
}