<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends MY_Controller {
	function __construct() {
		parent::__construct();

        $this->load->model('Home_model');
    }

    function edit() {
        $data["temp"] = "home/edit";
        $data["page_view"] = "home";
        $data["detail"] = $this->Home_model->get_content();
        $this->load->view("admin/index",$data);
    }

    function update() {
        $post_data = $this->input->post();
        if ( $post_data ) {

            $thumb_img = "";
            if( $_FILES ) {
                $target_dir = "assets/posts/";
                if( isset( $_FILES["thumb_img"] ) ) {
                    $thumb_img = $target_dir . basename($_FILES["thumb_img"]["name"]);
                    if( !file_exists($thumb_img) )
                        move_uploaded_file($_FILES["thumb_img"]["tmp_name"], $thumb_img);
                    $this->Home_model->resizeImage($thumb_img);
                }
                
            }

            $res = array();
            $update_data = array(
                'seo_description'   => $post_data['description'],
                'seo_keywords'      => $post_data['keywords'],
            );
            if( !empty($thumb_img) ) {
                $update_data["image"] = $thumb_img;
            }
            $this->Home_model->update_content($update_data);
            $res['code'] = 200;
            $res['message'] = "update successful";
            echo json_encode($res);
        }
    }

}
