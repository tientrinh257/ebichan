<?php
class Services_model extends MY_Model {
    function __construct() {
        parent::__construct();

        $this->table = "services";

    }

    function get_services() {
        $this->db->where("disable",0);
        
        $query = $this->db->get("$this->table");
        return $query->result_array();
    }

    function get_service($id) {
        $this->db->where("disable",0);
        $this->db->where("id",$id);
        $query = $this->db->get("$this->table");
        return $query->row_array();
    }

    function get_by_name($url) {
        $this->db->where("disable",0);
        $this->db->where("url",$url);
        $query = $this->db->get("$this->table");
        return $query->row_array();
    }

    function list_services() {
        $this->db->select("name,url");
        $this->db->where("disable",0);
        $this->db->where("menu_visible",1);
        $query = $this->db->get("$this->table");
        return $query->result_array();
	}
}