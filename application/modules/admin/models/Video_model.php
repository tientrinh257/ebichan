<?php
class Video_model extends MY_Model {
    function __construct() {
        parent::__construct();
        $this->table = "video";
    }

    function get_all_video_list($title="NULL") {
        if($title != "NULL") $this->db->like("title",$title);
        $this->db->where("disable",0);
        $this->db->order_by("create_datetime","DESC");
        $query = $this->db->get("$this->table");
        return $query->result_array();
    }

    function get_video($id) {
        $this->db->where("disable",0);
        $this->db->where("id",$id);
        $query = $this->db->get("$this->table");
        return $query->row_array();
    }
}