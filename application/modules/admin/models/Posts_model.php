<?php
class Posts_model extends MY_Model {
    function __construct() {
        parent::__construct();
        $this->table = "posts";
    }

    function get_limit_posts($limit,$offset) {
        $this->db->where("disable",0);
        $this->db->order_by("create_datetime","DESC");
        $this->db->limit($limit,$offset);
        return $this->db->get($this->table)->result_array();
    }

    function get_all_posts() {
        $this->db->where("disable",0);
        return $this->db->get($this->table)->num_rows();
    }

    function get_all_posts_list($title="NULL") {
        if($title != "NULL") $this->db->like("title",$title);
        $this->db->where("disable",0);
        $this->db->order_by("create_datetime","DESC");
        $query = $this->db->get("$this->table");
        return $query->result_array();
    }

    function get_posts_list($offset=0, $limit=6) {
        $this->db->where("disable",0);
        $this->db->limit($limit, $offset);
        $this->db->order_by("create_datetime","DESC");
        $query = $this->db->get("$this->table");
        return $query->result_array();
    }

    function get_side_post_list($offset=0, $limit=6) {
        $this->db->select("url,title,create_datetime");
        $this->db->where("disable",0);
        $this->db->limit($limit, $offset);
        $this->db->order_by("create_datetime","DESC");
        $query = $this->db->get("$this->table");
        return $query->result_array();
    }

    function get_posts($id) {
        $this->db->where("disable",0);
        $this->db->where("id",$id);
        $query = $this->db->get("$this->table");
        return $query->row_array();
    }

    function get_posts_by_name($url) {
        $this->db->where("disable",0);
        $this->db->where("url",$url);
        $query = $this->db->get("$this->table");
        return $query->row_array();
    }

    function get_posts_list_author($author,$offset=0, $limit=6) {
        $this->db->where("disable",0);
        $this->db->where("author",$author);
        $this->db->limit($limit, $offset);
        $this->db->order_by("create_datetime","DESC");
        $query = $this->db->get("$this->table");
        return $query->result_array();
    }
}