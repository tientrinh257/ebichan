<?php
class About_model extends MY_Model {
    function __construct() {
        parent::__construct();

        $this->table = "page_data";

    }

    function get_content() {
        $this->db->where("page","about");
        $query = $this->db->get("$this->table");
        return $query->row_array();
    }

    function update_content($data) {
        $where = array("page"=>"about");
        return $this->update_rule($where,$data);
    }
}