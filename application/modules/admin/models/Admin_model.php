<?php
class Admin_model extends MY_Model {
    function __construct() {
        parent::__construct();
    }

    function count_guitar() {
        return $this->db->where("disable",0)->get("guitar_list")->num_rows();
    }

    function count_category() {
        return $this->db->where("disable",0)->get("categories")->num_rows();
    }

    function count_collection() {
        return $this->db->where("disable",0)->get("collections")->num_rows();
    }
}