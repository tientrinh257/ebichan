<?php
class Message_model extends MY_Model {
    function __construct() {
        parent::__construct();

        $this->table = "messages";
    }

    function get_message_list($per_page, $offset, $status, $sort) {
        $this->db->order_by("viewed","ASC");
        if($status != "NULL")
            $this->db->where("viewed",$status);
        if($sort != "NULL")
            $this->db->order_by("create_datetime",$sort);
        $this->db->where("disable",0);
        $this->db->limit($per_page,$offset);
        $result = $this->db->get($this->table);
        return $result->result();
    }

    function get_message_count($status = "NULL") {
        if($status != "NULL")
            $this->db->where("viewed",$status);
        $this->db->where("disable",0);
        $result = $this->db->get($this->table);
        return $result->num_rows();
    }

    function get_message($id) {
        return $this->get_info($id);
    }

    function update_status($id) {
        return $this->update($id,array("viewed"=>"1"));
    }
}