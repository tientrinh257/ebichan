<h3 class="header-title">Add posts</h3>

<form id="add" method="post" action="<?= base_url('admin/posts/insert') ?>" autocomplete="off" enctype="multipart/form-data">
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Title</label>
        <div class="col-sm">
            <input type="text" class="form-control" name="title" required />
        </div>
    </div>
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Thumbnail image</label>
        <div class="col-sm">
            <input type="file" class="form-control" name="thumb_img" accept="image/*" required/>
        </div>
    </div>

    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Content</label>
        <div class="col-sm">
            <div class="toolbar-container sticky-top"></div>
            <div class="content-container">
                <div name="content" id="editor"></div>
            </div>
        </div>
    </div>
    <hr>
    <h3> SEO </h3>
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Keywords</label>
        <div class="col-sm">
            <textarea class="form-control" name="keywords" rows="3" required></textarea>
        </div>
    </div>
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Description</label>
        <div class="col-sm">
            <textarea class="form-control" name="description" rows="8" required></textarea>
        </div>
    </div>
    <div class="form-group row ml-0 mr-0 pl-15 pr-15 justify-content-between">
        <a class="btn btn-secondary" href="<?= admin_url('services/list') ?>">Back</a>
        <input type="submit" class="btn btn-info" value="Save" />
    </div>

</form>
<input type="hidden" id="main_url" value="admin/posts/list" ?>
<script src="<?= public_url('ckeditor5/ckeditor.js') ?>"></script>
<script src="<?= public_url('js/posts/create.js').'?'. JS_FILE_VERSION ?>"></script>