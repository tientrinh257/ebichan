<h3 class="header-title">Edit posts</h3>

<form id="edit" method="post" action="<?= base_url('admin/posts/update') ?>" autocomplete="off" enctype="multipart/form-data">
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Title</label>
        <div class="col-sm">
            <input type="text" class="form-control" name="title" value="<?php echo $posts_detail["title"] ?>" required />
        </div>
    </div>
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Thumbnail Image</label>
        <div class="col-sm">
            <?php if ( empty($posts_detail["image"] )) { ?>
                <input type="file" class="form-control" name="thumb_img" accept="image/*" />
            <?php } else { ?>
                <img src="<?= base_url($posts_detail["image"]) ?>" height="100" width="auto" class="thumb_img" />
                <button type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            <?php } ?>
        </div>
    </div>
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Content</label>
        <div class="col-sm">
            <div class="toolbar-container sticky-top"></div>
            <div class="content-container">
                <div name="content" id="editor"><?php echo $posts_detail["content"] ?></div>
            </div>
        </div>
    </div>
    <hr>
    <h3> SEO </h3>
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Keywords</label>
        <div class="col-sm">
            <textarea class="form-control" name="keywords" rows="3" required><?php echo $posts_detail["seo_keywords"] ?></textarea>
        </div>
    </div>
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Description</label>
        <div class="col-sm">
            <textarea class="form-control" name="description" rows="8" required><?php echo $posts_detail["seo_description"] ?></textarea>
        </div>
    </div>
    <div class="form-group row ml-0 mr-0 pl-15 pr-15 justify-content-between">
        <input type="hidden" id="posts_id" value="<?php echo $posts_detail["id"] ?>" name="posts_id" />
        <a class="btn btn-secondary" href="<?= admin_url('posts/list') ?>">Back</a>
        <input type="submit" class="btn btn-info" value="Save" />
    </div>

</form>
<input type="hidden" id="main_url" value="admin/posts/list" ?>
<script src="<?= public_url('ckeditor5/ckeditor.js') ?>"></script>
<script src="<?= public_url('js/posts/edit.js').'?'. JS_FILE_VERSION ?>"></script>