<style>
    .ck-editor__editable_inline {
        min-height: 200px;
    }
</style>

<h3 class="header-title">Edit product</h3>

<form id="edit" method="post" action="<?= base_url('admin/product/update') ?>" autocomplete="off" enctype="multipart/form-data">
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Title</label>
        <div class="col-sm">
            <input type="text" class="form-control" name="title" value="<?php echo $product_detail["title"] ?>" required />
        </div>
    </div>
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Thumbnail Image</label>
        <div class="col-sm">
            <?php if ( empty($product_detail["image"] )) { ?>
                <input type="file" class="form-control" name="thumb_img" accept="image/*" />
            <?php } else { ?>
                <img src="<?= base_url($product_detail["image"]) ?>" height="100" width="auto" class="thumb_img" />
                <button type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            <?php } ?>
        </div>
    </div>
    <!-- <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Content</label>
        <div class="col-sm">
            <textarea name="content" id="editor" class="form-control" rows="5"><?php echo $product_detail["content"] ?></textarea>
        </div>
    </div> -->

    <div class="form-group row ml-0 mr-0 pl-15 pr-15 justify-content-between">
        <input type="hidden" id="product_id" value="<?php echo $product_detail["id"] ?>" name="product_id" />
        <a class="btn btn-secondary" href="<?= admin_url('product/list') ?>">Back</a>
        <input type="submit" class="btn btn-info" value="Save" />
    </div>

</form>
<input type="hidden" id="main_url" value="admin/product/list" ?>
<!-- <script src="<?= public_url('js/common/ckeditor.js') ?>"></script> -->
<script src="<?= public_url('js/admin/video/edit.js').'?'. JS_FILE_VERSION ?>"></script>