<style>
    .ck-editor__editable_inline {
        min-height: 200px;
    }
</style>

<h3 class="header-title">Add product</h3>

<form id="add" method="post" action="<?= base_url('admin/product/insert') ?>" autocomplete="off" enctype="multipart/form-data">
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Title</label>
        <div class="col-sm">
            <input type="text" class="form-control" name="title" required />
        </div>
    </div>
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Thumbnail image</label>
        <div class="col-sm">
            <input type="file" class="form-control" name="thumb_img" accept="image/*" required/>
        </div>
    </div>

    <!-- <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Content</label>
        <div class="col-sm">
            <textarea name="content" id="editor" class="form-control" rows="20"></textarea>
        </div>
    </div> -->

    <div class="form-group row ml-0 mr-0 pl-15 pr-15 justify-content-between">
        <a class="btn btn-secondary" href="<?= admin_url('product/list') ?>">Back</a>
        <input type="submit" class="btn btn-info" value="Save" />
    </div>

</form>
<input type="hidden" id="main_url" value="admin/product/list" ?>
<!-- <script src="<?= public_url('js/common/ckeditor.js') ?>"></script> -->
<script src="<?= public_url('js/admin/video/create.js').'?'. JS_FILE_VERSION ?>"></script>