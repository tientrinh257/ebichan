<!-- CONTENT -->

<h3 class="header-title">Message list</h3>
<div class="container">
<div class="row">
        <div class="col-md-8">
        <?php 
        $attr = array("class" => "form-horizontal", "role" => "form");
        echo form_open("admin/Message/search", $attr);?>
            <div class="form-group">
                <div class="col-md-6">
                    <select name="message_status" class="form-control" required>
                        <option disabled selected>Search for message status...</option>
                        <option value="1" <?php if(set_value("message_status") == "1") echo 'selected' ?>>Viewed</option>
                        <option value="0" <?php if(set_value("message_status") == "0") echo 'selected' ?>>New</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <select name="date_sort" class="form-control" required>
                        <option disabled selected>Search for date sort...</option>
                        <option value="ASC" <?php if(set_value("date_sort") == "ASC") echo 'selected' ?>>ASC</option>
                        <option value="DESC" <?php if(set_value("date_sort") == "DESC") echo 'selected' ?> >DESC</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <input id="btn_search" name="btn_search" type="submit" class="btn btn-danger" value="Search" />
                    <a href="<?php echo base_url(). "admin/Message/show"; ?>" class="btn btn-primary">Show All</a>
                </div>
            </div>
        <?php echo form_close(); ?>
        </div>
    </div>
    <table class="table table-light table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th rowspan="2" scope="col">#</th>
                <th rowspan="2" scope="col">Name</th>
                <th rowspan="2" scope="col">Send date</th>
                <th rowspan="2" scope="col">Status</th>
                <th colspan="2" scope="col">Action</th>
            </tr>
            <tr>
                <th scope="col">View</th>
                <th scope="col">Delete</th>
            </tr>
        </thead>
        <tbody>
        <?php if(empty($message_list)) : ?>
            <tr>
                <td colspan="6" align="center">No data</td>
            </tr>
        <?php else : ?>
            <?php foreach ($message_list as $key => $value) { ?>
                <tr>
                    <th scope="row"><?php echo $key + $offset + 1 ?></th>
                    <td><?= $value->name ?></td>
                    <td><?= $value->create_datetime ?></td>
                    <?php $status = $value->viewed == 1 ? "Viewed" : "New"; ?>
                    <td><?= $status ?></td>
                    <td align="center">
                        <a class="btn btn-info edit" href="<?php echo admin_url('Message/view/'.$value->id)?>">View</a>
                    </td>
                    <td align="center">
                        <input type="button" class="btn btn-danger delete" data-url="<?= admin_url('Message/delete/'.$value->id)?>" value="Delete" />
                    </td>
                </tr>
            <?php } ?>
        <?php endif ?>    
        </tbody>
    </table>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <?php echo $pagination; ?>
    </div>
</div>

<script src="<?= public_url('js/admin/message/show.js')?>"></script>
<!-- /CONTENT -->
