<h3 class="header-title">Message</h3>

<form>
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Name</label>
        <div class="col-sm">
            <input type="text" class="form-control" name="name" value="<?= $message_detail['name'] ?>" disabled />
        </div>
    </div>

    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Email</label>
        <div class="col-sm">
            <input type="email" class="form-control" name="email" value="<?= $message_detail['email'] ?>" disabled />
        </div>
    </div>

    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Telephone</label>
        <div class="col-sm">
            <input type="tel" class="form-control" name="telephone" value="<?= $message_detail['telephone'] ?>" disabled />
        </div>
    </div>

    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Message</label>
        <div class="col-sm">
            <pre>
                <p class="message"><?= $message_detail['message'] ?></p>
            </pre>
        </div>
    </div>

</form>
<div>
    <a class="btn btn-secondary" href="<?php echo admin_url('Message/show')?>">Back</a>
</div>