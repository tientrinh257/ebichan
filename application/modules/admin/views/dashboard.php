<!-- Header -->
<header class="w3-container" style="padding-top:22px">
    <h5><b><i class="fa fa-dashboard"></i> My Dashboard</b></h5>
</header>

<div class="w3-row-padding w3-margin-bottom">
    <!-- <a class="w3-quarter" href="<?= admin_url('Message/show')?>">
        <div class="w3-container w3-red w3-padding-16">
            <div class="w3-left"><i class="fa fa-comment fa-fw w3-xxxlarge"></i></div>
            <div class="w3-right">
                <h3><?= $count_message ?></h3>
            </div>
            <div class="w3-clear"></div>
            <h4>Tin nhắn</h4>
        </div>
    </a> -->
    <a class="w3-quarter" href="<?= base_url('admin/Services/list')?>">
        <div class="w3-container w3-teal w3-padding-16">
            <div class="w3-left"><i class="fas fa-hands-helping fa-fw w3-xxxlarge"></i></div>
            <div class="w3-right">
                <h3></h3>
            </div>
            <div class="w3-clear"></div>
            <h4>Dịch vụ</h4>
        </div>
    </a>


    <a class="w3-quarter" href="<?= base_url('admin/posts/list')?>">
        <div class="w3-container w3-blue w3-padding-16">
            <div class="w3-left"><i class="fas fa-newspaper fa-fw w3-xxxlarge"></i></div>
            <div class="w3-right">
                <h3></h3>
            </div>
            <div class="w3-clear"></div>
            <h4>Bài viết</h4>
        </div>
    </a>


    <a class="w3-quarter" href="<?= base_url('')?>">
        <div class="w3-container w3-orange w3-text-white w3-padding-16">
            <div class="w3-left"><i class="fa fa-globe-americas fa-fw w3-xxxlarge"></i></div>
            <div class="w3-right">
                <h3></h3>
            </div>
            <div class="w3-clear"></div>
            <h4>Website</h4>
        </div>
    </a>

</div>