<h3 class="header-title">Account <span style="color:red"><?php echo $this->session->flashdata('mes') ?></h3>

<form id="edit" method="post" action="<?= base_url('admin/account/update') ?>" autocomplete="off">
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Username</label>
        <div class="col-sm">
            <input type="text" class="form-control" name="user_name" value="<?php echo $info["user_name"] ?>" required readonly />
        </div>
    </div>

    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Password</label>
        <div class="col-sm">
            <input type="password" class="form-control" name="user_password" value="" required />
        </div>
    </div>

    <div class="form-group row ml-0 mr-0 pl-15 pr-15 justify-content-between">
        <input type="hidden" id="service_id" value="<?php echo $info["id"] ?>" name="user_id" />
        <a class="btn btn-secondary" href="<?= admin_url('account/list') ?>">Back</a>
        <input type="submit" class="btn btn-info" value="Save" />
    </div>

</form>
<input type="hidden" id="main_url" value="admin/account/list" ?>
<script src="<?= public_url('js/account/edit.js').'?'. JS_FILE_VERSION ?>"></script>