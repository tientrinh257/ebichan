<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px;" id="mySidebar"><br>
	<div class="w3-container w3-row">
		<div class="w3-col-12 s4">
      		<img src="/public/css/admin/image/logo.png" class="w3-margin-right" width="100">
    	</div>
    	<div class="w3-col-12 s8 w3-bar">
			<span>Welcome, <strong>AcomAgencyVN</strong></span><br>
    	</div>
  	</div>
  	<hr>
	<div class="main-menu">
		<ul class="list-group">
			<li class="menu-item">
				<a href="<?= admin_url()?>" class="list-group-item list-group-item-action" data-name="dashboard"><i class="fa fa-tachometer-alt fa-fw"></i>  Dashboard</a>
			</li>
			<li class="menu-item">
				<a href="<?php echo base_url('admin/account/list') ?>" class="list-group-item list-group-item-action" data-name="account"><i class="fa fa-user fa-fw"></i>  Tài khoản</a>
			</li>
			<li class="menu-item">
				<a href="<?php echo base_url('admin/home/edit') ?>" class="list-group-item list-group-item-action" data-name="home"><i class="fas fa-home fa-fw"></i> Trang chủ</a>
			</li>
			<li class="menu-item">
				<a href="<?php echo base_url('admin/about/edit') ?>" class="list-group-item list-group-item-action" data-name="about"><i class="far fa-address-card fa-fw"></i> Về chúng tôi</a>
			</li>
			<li class="menu-item">
				<a href="<?php echo base_url('admin/services/list') ?>" class="list-group-item list-group-item-action" data-name="services"><i class="fas fa-hands-helping fa-fw"></i>  Dịch vụ</a>
			</li>
			<li class="menu-item">
				<a href="<?php echo base_url('admin/posts/list') ?>" class="list-group-item list-group-item-action" data-name="posts"><i class="fas fa-newspaper fa-fw"></i>  Bài viết</a>
			</li>
			<li class="menu-item">
				<a href="<?php echo base_url('admin/product/list') ?>" class="list-group-item list-group-item-action" data-name="product"><i class="fas fa-brain fa-fw"></i>  Sản phẩm</a>
			</li>
			<li class="menu-item">
				<a href="<?php echo base_url('admin/video/list') ?>" class="list-group-item list-group-item-action" data-name="video"><i class="fas fa-photo-video fa-fw"></i>  Video</a>
			</li>
			<li class="menu-item">
				<a target="_blank" href="<?php echo base_url() ?>" class="list-group-item list-group-item-action"><i class="fa fa-globe-americas fa-fw"></i>  Website</a>
			</li>
		</ul>
    
	</div>
</nav>
<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>
<input type="hidden" id="current_menu" value="<?= $page_view ?>" />
<script src="<?= public_url('js/admin/menu.js').'?'. JS_FILE_VERSION ?>"></script>