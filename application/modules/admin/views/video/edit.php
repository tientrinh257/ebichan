<h3 class="header-title">Edit video</h3>

<form id="edit" method="post" action="<?= base_url('admin/video/update') ?>" autocomplete="off" enctype="multipart/form-data">
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Title</label>
        <div class="col-sm">
            <input type="text" class="form-control" name="title" value="<?php echo $video_detail["title"] ?>" required />
        </div>
    </div>
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Thumbnail Image</label>
        <div class="col-sm">
            <?php if ( empty($video_detail["image"] )) { ?>
                <input type="file" class="form-control" name="thumb_img" accept="image/*" />
            <?php } else { ?>
                <img src="<?= base_url($video_detail["image"]) ?>" height="100" width="auto" class="thumb-img" />
                <button type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            <?php } ?>
        </div>
    </div>
    <div class="form-group row ml-0 mr-0">
        <label class="col-sm-3 col-form-label">Link video</label>
        <div class="col-sm">
            <input type="text" class="form-control" name="link-video" value="<?php echo $video_detail["link_video"] ?>" required />
        </div>
    </div>

    <div class="form-group row ml-0 mr-0 pl-15 pr-15 justify-content-between">
        <input type="hidden" id="video-id" value="<?php echo $video_detail["id"] ?>" name="video-id" />
        <a class="btn btn-secondary" href="<?= admin_url('video/list') ?>">Back</a>
        <input type="submit" class="btn btn-info" value="Save" />
    </div>

</form>
<input type="hidden" id="main_url" value="admin/video/list" ?>
<script src="<?= public_url('js/admin/video/edit.js').'?'. JS_FILE_VERSION ?>"></script>