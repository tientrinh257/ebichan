<!-- CONTENT -->

<h3 class="header-title">Video list</h3>
<div class="row">
    <div class="col-md-8">
    <?php 
    $attr = array("class" => "form-horizontal", "role" => "form");
    echo form_open("admin/video/search", $attr);?>
        <div class="form-group">
            <div class="col-md-6">
                <input class="form-control" name="video-title" placeholder="Search for video title..." type="text" value="<?php echo set_value('video-title'); ?>" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <input id="btn_search" name="btn_search" type="submit" class="btn btn-danger" value="Search" />
                <a href="<?php echo base_url(). "admin/video/list"; ?>" class="btn btn-primary">Show All</a>
                <a class="btn btn-info" href="<?php echo base_url('admin/video/create')?>">Add</a>
            </div>
        </div>
    <?php echo form_close(); ?>
    </div>
    <div class="col-md-4">
    </div>
</div>
<div class="container">
    <table class="table table-light table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th rowspan="2" scope="col">#</th>
                <th rowspan="2" scope="col">Title</th>
                <th rowspan="2" scope="col">Thumbnail Image</th>
                <th rowspan="2" scope="col">Modify date</th>
                <th colspan="3" scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php if(empty($video_list)) : ?>
                <tr>
                    <td colspan="8" class="text-center">No data</td>
                </tr>
            <?php else : ?>
                <?php foreach ($video_list as $key => $value) : ?>
                    <tr>
                        <th scope="row"><?php echo $key + 1 ?></th>
                        <td><?php echo $value["title"] ?></td>
                        <td><img src="<?= base_url($value["image"]) ?>" height="100" width="auto" /></td>
                        <td><?php echo $value["lastup_datetime"] ?></td>
                        <td class="text-center">
                            <a class="btn btn-info edit" href="<?php echo base_url('admin/video/edit/'.$value["id"])?>">Edit</a>
                        </td>
                        <td class="text-center">
                            <input type="button" class="btn btn-danger delete" value="Delete" data-url="<?= base_url('admin/video/delete/'.$value["id"])?>" />
                        </td>
                    </tr>
                <?php endforeach ?>
            <?php endif ?>
        </tbody>
    </table>
</div>
<hr>
<script src="<?= public_url('js/admin/video/list.js').'?'. JS_FILE_VERSION ?>"></script>
<!-- /CONTENT -->
