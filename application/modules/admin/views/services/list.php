<!-- CONTENT -->

<h3 class="header-title">Services list</h3>
<div class="container">
    <table class="table table-light table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th rowspan="2" scope="col">#</th>
                <th rowspan="2" scope="col">Name</th>
                <th rowspan="2" scope="col">Modify date</th>
                <th colspan="3" scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php if(empty($services_list)) : ?>
                <tr>
                    <td colspan="5" class="text-center">No data</td>
                </tr>
            <?php else : ?>
                <?php foreach ($services_list as $key => $value) : ?>
                    <tr>
                        <th scope="row"><?php echo $key + 1 ?></th>
                        <td><?php echo $value["name"] ?></td>
                        <td><?php echo $value["lastup_datetime"] ?></td>
                        <td class="text-center">
                            <a class="btn btn-info edit" href="<?php echo base_url('admin/Services/edit/'.$value["id"])?>">Edit</a>
                        </td>
                        <td class="text-center">
                            <input type="button" class="btn btn-danger delete" value="Delete" data-url="<?= base_url('admin/Services/delete/'.$value["id"])?>" />
                        </td>
                    </tr>
                <?php endforeach ?>
            <?php endif ?>
        </tbody>
    </table>
</div>
<hr>
<script src="<?= public_url('js/posts/list.js').'?'. JS_FILE_VERSION ?>"></script>
<!-- /CONTENT -->
