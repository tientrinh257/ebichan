<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>AcomAgencyVN | Admin</title>
<link rel="stylesheet" href="<?= public_url('css/common/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?php echo public_url('css/common/fontawesome.css') ?>">
<link rel="stylesheet" href="<?php echo public_url('css/common/w3.css') ?>">
<link rel="stylesheet" href="<?php echo public_url('css/admin/style.css') ?>">
<link rel="stylesheet" href="<?= public_url('css/common/classiceditor.css') ?>">
<link rel="stylesheet" href="<?= public_url('css/common/mediaembed.css') ?>">
<link rel="stylesheet" href="<?= public_url('css/common/mediaembedediting.css') ?>">
<script type="text/javascript" src="<?php echo public_url('js/common/jquery-3.5.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo public_url('js/common/bootstrap.bundle.min.js')?>"></script>
<script type="text/javascript" src="<?php echo public_url('js/admin/main.js').'?'. JS_FILE_VERSION ?>"></script>