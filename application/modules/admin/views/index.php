<!DOCTYPE html>
<html>
<?php $this->load->view('head') ?>

<body class="w3-light-grey">
	<input type="hidden" id="base_url" value="<?= base_url() ?>" />
	<?php $this->load->view('modal') ?>
	<!-- Top container -->
	<div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
		<button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
		<span class="w3-bar-item w3-right">
			<a href="<?= admin_url('logout') ?>"><i class="fas fa-power-off"></i> Logout</a>
		</span>
	</div>

	<?php $this->load->view("menu"); ?>

	<!-- !PAGE CONTENT! -->
	<div class="w3-main pd-10" style="margin-left:300px;margin-top:43px;min-height:400px">

		<?php if(isset($temp) && !empty($temp)) { 
			$this->load->view($temp);
		} ?>

	<!-- End page content -->
	</div>
	<div id="footer" class="text-center">
		<div id="waitting">
			<div class="sk-fading-circle fa-4x">
				<i class="fas fa-cog fa-spin"></i>
			</div>
		</div>
	</div>
</body>
</html>
