<?php
$config['use_page_numbers'] = TRUE;
$config['full_tag_open'] = '<nav class="pagination">';
$config['full_tag_close'] = '</nav>';
$config['first_link'] = false;
$config['last_link'] = false;
$config['prev_link'] = '< Previous';
$config['next_link'] = 'Next >';
$config['cur_tag_open'] = '<span class="active page-numbers">';
$config['cur_tag_close'] = '</span>';
$config['attributes'] = array('class' => 'page-numbers');